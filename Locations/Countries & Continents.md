# Continents
-------

| Kanji | Katakana       | Romanji     | English   |
|-------|----------------|-------------|-----------|
|       | アフリカ       | afurika     | Africa    |
|       | アジア         | ajia        | Asia      |
|       | オーストラリア | o-sutoraria | Australia |
|       | ヨーロッパ     | Yōroppa     | Europe    |

# Countries
-------

| Kanji | Katakana/Hiragana | Romanji        | English         |
|-------|-------------------|----------------|-----------------|
|       | アメリカ          | amerika        | America (US)    |
|       | ブラジル          | burajiru       | Brazil          |
|       | カナダ            | kanada         | Canada          |
| 中国  | ちゅうごく        | chuugoku       | China           |
|       | フランス          | furansu        | France          |
|       | ドイツ            | doitsu         | Germany         |
|       | インド            | indo           | India           |
|       | アイルランド      | Airurando      | Ireland         |
|       | イタリア          | itaria         | Italy           |
| 日本  | にほん [にっぽん] | nihon [nippon] | Japan           |
|       | マドリイド        | madoriido      | Madrid          |
|       | メキシコ          | mekishiko      | Mexico          |
|       | ロシア            | roshia         | Russia          |
|       | スペイン          | supein         | Spain           |
|       | フィリピン        | firipin        | The Philippines |
|       | イギリス          | igirisu        | United Kingdom  |


    

| Kanji 	| Katakana       	| Romanji     	| English   	|
|-------	|----------------	|-------------	|-----------	|
|       	| アフリカ       	| afurika     	| Africa    	|
|       	| アジア         	| ajia        	| Asia      	|
|       	| オーストラリア 	| o-sutoraria 	| Australia 	|
|       	| ヨーロッパ     	| Yōroppa     	| Europe    	|




